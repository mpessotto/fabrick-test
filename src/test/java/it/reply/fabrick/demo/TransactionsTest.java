package it.reply.fabrick.demo;

import it.reply.fabrick.demo.delegate.AccountDelegate;
import it.reply.fabrick.demo.fabrick.FabrickApis;
import it.reply.fabrick.demo.fabrick.model.TransactionItem;
import it.reply.fabrick.demo.fabrick.model.TransactionResponse;
import it.reply.fabrick.demo.fabrick.model.TransactionType;
import it.reply.fabrick.demo.model.GetTransactionResponse;
import it.reply.fabrick.demo.model.GetTransactionsRequest;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest
class TransactionsTest {

    @Autowired
    private AccountDelegate accountDelegate;

    @MockBean
    private FabrickApis fabrickApis;

    @Test
    public void testGetCashAccountTransactions() throws Exception {

        String accountId = "00000";
        String fromAccountingDate = "2019-07-01";
        String toAccountingDate = "2019-12-31";
        GetTransactionsRequest request = new GetTransactionsRequest();
        request.setFromDate(fromAccountingDate);
        request.setToDate(toAccountingDate);
        List<TransactionItem> expectedTransactions = generateTrItems();
        TransactionResponse fabrickTransactionResponse = new TransactionResponse();
        fabrickTransactionResponse.setList(expectedTransactions);
        GetTransactionResponse expectedResponse = new GetTransactionResponse();
        expectedResponse.setTransactionItemList(expectedTransactions);
        expectedResponse.setTransactionTotal(expectedTransactions.size());

        Mockito.when(fabrickApis.getTransactions(accountId, request.getFromDate(), request.getToDate()))
                .thenReturn(fabrickTransactionResponse);


        ResponseEntity<GetTransactionResponse> response = accountDelegate.getCashAccountTransactions(accountId,request);
        assertEquals(response.getBody().getTransactionItemList(),expectedResponse.getTransactionItemList());


    }

    private List<TransactionItem> generateTrItems() {
        List<TransactionItem> list = new ArrayList<>();

        TransactionItem transactionItem1 = new TransactionItem();
        transactionItem1.setTransactionId("1329140278001");
        transactionItem1.setOperationId("19000017094683");
        transactionItem1.setAccountingDate(LocalDate.parse("2019-02-01"));
        transactionItem1.setValueDate(LocalDate.parse("2019-02-01"));
        TransactionType type1 = new TransactionType();
        type1.setEnumeration("GBS_TRANSACTION_TYPE");
        type1.setValue("GBS_ACCOUNT_TRANSACTION_TYPE_0034");
        transactionItem1.setType(type1);
        transactionItem1.setAmount(new BigDecimal("89.00"));
        transactionItem1.setCurrency("EUR");
        transactionItem1.setDescription("GC LUCA TERRIBILE DA 03268.22300 DATA ORDINE 01022019 COPERTURA SPESE");

        TransactionItem transactionItem2 = new TransactionItem();
        transactionItem2.setTransactionId("398894");
        transactionItem2.setOperationId("00000000398894");
        transactionItem2.setAccountingDate(LocalDate.parse("2019-01-31"));
        transactionItem2.setValueDate(LocalDate.parse("2019-02-01"));
        TransactionType type2 = new TransactionType();
        type2.setEnumeration("GBS_TRANSACTION_TYPE");
        type2.setValue("GBS_ACCOUNT_TRANSACTION_TYPE_0050");
        transactionItem2.setType(type2);
        transactionItem2.setAmount(new BigDecimal("-95.73"));
        transactionItem2.setCurrency("EUR");
        transactionItem2.setDescription("PD VISA CORPORATE 12");

        list.add(transactionItem1);
        list.add(transactionItem2);

        return list;
    }


}
