package it.reply.fabrick.demo;

import it.reply.fabrick.demo.fabrick.model.BalanceResponse;
import it.reply.fabrick.demo.fabrick.model.TransactionItem;
import it.reply.fabrick.demo.fabrick.model.TransactionType;
import it.reply.fabrick.demo.apis.interfaces.AccountApi;
import it.reply.fabrick.demo.model.GetTransactionResponse;
import it.reply.fabrick.demo.model.GetTransactionsRequest;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class FabrickDemoTests {

    @Autowired
    private MockMvc mockMvc; // MockMvc per simulare le richieste HTTP

    @MockBean
    private AccountApi accountApi; // Mock del controller

    @Test
    public void testGetCashAccountBalance() throws Exception {
        // Dati di esempio
        String accountId = "123";
        BalanceResponse expectedResponse = new BalanceResponse();

        expectedResponse.setCurrency("EUR");
        expectedResponse.setBalance(new BigDecimal("29.64"));
        expectedResponse.setAvailableBalance(new BigDecimal("29.64"));
        expectedResponse.setDate(LocalDate.parse("2023-08-01"));

        // Configura il comportamento del mock del controller
        Mockito.when(accountApi.getCashAccountBalance(accountId)).thenReturn(new ResponseEntity<>(expectedResponse, HttpStatus.OK));

        // Esegui la richiesta GET al controller e verifica la risposta
        mockMvc.perform(get("/account/{accountId}/balance", accountId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.currency", is(expectedResponse.getCurrency())))
                .andExpect(jsonPath("$.balance", is(expectedResponse.getBalance().doubleValue())))
                .andExpect(jsonPath("$.availableBalance", is(expectedResponse.getAvailableBalance().doubleValue())));
    }




}
