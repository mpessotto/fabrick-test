package it.reply.fabrick.demo.dto;

import it.reply.fabrick.demo.fabrick.model.CreateMoneyTransferRequest;

public class CreateMoneyTransferRequestDto {
    private String accountId;
    private CreateMoneyTransferRequest request;

    public CreateMoneyTransferRequestDto(String accountId, CreateMoneyTransferRequest request) {
        this.accountId = accountId;
        this.request = request;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public CreateMoneyTransferRequest getRequest() {
        return request;
    }

    public void setRequest(CreateMoneyTransferRequest request) {
        this.request = request;
    }
}
