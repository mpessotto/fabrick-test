package it.reply.fabrick.demo;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.servers.Server;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = { "it.reply.fabrick.demo"})
public class FabrickDemo implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(FabrickDemo.class, args);
	}

	@Override
	public void run(String... arg0) {

	}

}

