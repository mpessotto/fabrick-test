package it.reply.fabrick.demo.apis.controller;

import it.reply.fabrick.demo.fabrick.model.BalanceResponse;
import it.reply.fabrick.demo.apis.interfaces.AccountApi;
import it.reply.fabrick.demo.delegate.AccountDelegate;
import it.reply.fabrick.demo.model.GetTransactionResponse;
import it.reply.fabrick.demo.model.GetTransactionsRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountApiController implements AccountApi {


    private AccountDelegate accountDelegate;

    public AccountApiController(@Autowired AccountDelegate accountDelegate){
        this.accountDelegate = accountDelegate;
    }

    @Override
    public ResponseEntity<BalanceResponse> getCashAccountBalance(String accountId) throws Exception {
        return accountDelegate.getCashAccountBalance(accountId);
    }

    @Override
    public ResponseEntity<GetTransactionResponse> getCashAccountTransactions(String accountId, GetTransactionsRequest body) throws Exception {
        return accountDelegate.getCashAccountTransactions(accountId,body);
    }


}
