
package it.reply.fabrick.demo.apis.interfaces;

import it.reply.fabrick.demo.fabrick.model.BalanceResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import it.reply.fabrick.demo.model.GetTransactionResponse;
import it.reply.fabrick.demo.model.GetTransactionsRequest;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


@Validated
@RequestMapping(value = "/account/{accountId}")
@RestController
public interface AccountApi {

    @Operation(summary = "returns cash account balance", description = "By passing in the account ID, it retrieves the balance of that cash account ", tags = {"Cash Account"})
    @GetMapping(value = "/balance",
            produces = {"application/json"})
    ResponseEntity<BalanceResponse> getCashAccountBalance(
            @Parameter(in = ParameterIn.PATH, description = "the ID of the account", required = true, schema = @Schema()) @PathVariable("accountId") String accountId) throws Exception;

    @Operation(summary = "returns cash account transactions", description = "By passing in the account ID and two dates, it returns the list of transactions made between those dates", tags = {"Cash Account"})
    @PostMapping(value = "/transactions",
            produces = {"application/json"}, consumes = {"application/json"})
    ResponseEntity<GetTransactionResponse> getCashAccountTransactions(
            @Parameter(in = ParameterIn.PATH, description = "the ID of the account", required = true, schema = @Schema()) @PathVariable("accountId") String accountId,
            @Parameter(in = ParameterIn.DEFAULT) @Valid @RequestBody GetTransactionsRequest body) throws Exception;
}

