package it.reply.fabrick.demo.apis.controller;

import it.reply.fabrick.demo.fabrick.model.CreateMoneyTransferRequest;
import it.reply.fabrick.demo.fabrick.model.CreateMoneyTransferResponse;
import it.reply.fabrick.demo.apis.interfaces.MoneyTransferApi;
import it.reply.fabrick.demo.delegate.MoneyTransferDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MoneyTransferController implements MoneyTransferApi {

    private MoneyTransferDelegate moneyTransferDelegate;

    public MoneyTransferController(@Autowired MoneyTransferDelegate moneyTransferDelegate){
        this.moneyTransferDelegate = moneyTransferDelegate;
    }

    public ResponseEntity<CreateMoneyTransferResponse> createMoneyTransfer(String accountId,CreateMoneyTransferRequest body) throws Exception {
        return this.moneyTransferDelegate.createMoneyTransfer(accountId,body);
    }

}
