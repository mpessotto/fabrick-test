package it.reply.fabrick.demo.fabrick.model;

import java.util.List;

public class TransactionResponse {
    private List<TransactionItem> list;

    public List<TransactionItem> getList() {
        return list;
    }

    public void setList(List<TransactionItem> list) {
        this.list = list;
    }
}
