package it.reply.fabrick.demo.fabrick;


import it.reply.fabrick.demo.fabrick.model.BalanceResponse;
import it.reply.fabrick.demo.fabrick.model.CreateMoneyTransferRequest;
import it.reply.fabrick.demo.fabrick.model.CreateMoneyTransferResponse;
import it.reply.fabrick.demo.fabrick.model.TransactionResponse;
import it.reply.fabrick.demo.fabrick.model.fabrickresponse.FabrickBalanceResponse;
import it.reply.fabrick.demo.fabrick.model.fabrickresponse.FabrickCreateMoneyTransferResponse;
import it.reply.fabrick.demo.fabrick.model.fabrickresponse.FabrickTransactionsResponse;
import it.reply.fabrick.demo.exception.FabrickException;
import it.reply.fabrick.demo.utils.properties.MicroserviceProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;


@Service
public class FabrickApis {
    private final RestTemplate restTemplate;

    @Autowired
    private MicroserviceProperties microserviceProperties;

    private static final Logger log = LoggerFactory.getLogger(FabrickApis.class);


    public FabrickApis(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public BalanceResponse getBalance(String accountId) throws FabrickException {
        String url = UriComponentsBuilder.fromHttpUrl(microserviceProperties.getFabrickBalanceUri()).buildAndExpand(accountId).toUriString();
        HttpHeaders httpHeaders = createHeaders(url);
        HttpEntity<Void> request = new HttpEntity<>(httpHeaders);
        ResponseEntity<FabrickBalanceResponse> httpResponse = restTemplate.exchange(url, HttpMethod.GET, request, FabrickBalanceResponse.class);
        if (httpResponse.getStatusCode().is2xxSuccessful() && "OK".equals(httpResponse.getBody().getStatus())) {
            return httpResponse.getBody().getPayload();
        }
        throw new FabrickException("Error in Fabrick Response");
    }

    public TransactionResponse getTransactions(String accountId, String fromAccountingDate, String toAccountingDate) throws FabrickException {
        String url = UriComponentsBuilder.fromHttpUrl(microserviceProperties.getFabrickTransactionsUri())
                .queryParam("fromAccountingDate", fromAccountingDate)
                .queryParam("toAccountingDate", toAccountingDate)
                .buildAndExpand(accountId).toUriString();
        HttpHeaders httpHeaders = createHeaders(url);
        HttpEntity<Void> request = new HttpEntity<>(httpHeaders);
        ResponseEntity<FabrickTransactionsResponse> httpResponse = restTemplate.exchange(url, HttpMethod.GET, request, FabrickTransactionsResponse.class);
        if (httpResponse.getStatusCode().is2xxSuccessful() && "OK".equals(httpResponse.getBody().getStatus())) {
            return httpResponse.getBody().getPayload();
        }
        throw new FabrickException("Error in Fabrick Response");
    }

    public CreateMoneyTransferResponse createMoneyTransfer(String accountId, CreateMoneyTransferRequest body) throws FabrickException {
        String url = UriComponentsBuilder.fromHttpUrl(microserviceProperties.getFabrickMoneyTransfersUri()).buildAndExpand(accountId).toUriString();;
        HttpHeaders httpHeaders = createHeaders(url);
        HttpEntity<CreateMoneyTransferRequest> request = new HttpEntity<>(body, httpHeaders);
        ResponseEntity<FabrickCreateMoneyTransferResponse> httpResponse = restTemplate.exchange(url,HttpMethod.POST,request, FabrickCreateMoneyTransferResponse.class);
        if (httpResponse.getStatusCode().is2xxSuccessful() && "OK".equals(httpResponse.getBody().getStatus())) {
            return httpResponse.getBody().getPayload();
        }
        throw new FabrickException("Error in Fabrick Response");
    }

    private HttpHeaders createHeaders(String url) {
        HttpHeaders headers = new HttpHeaders();
        try {
            headers.set("host", getHost(url));
        } catch (URISyntaxException e) {
            log.error("Error generating host header. Cause is: " + e.getLocalizedMessage());
        }

        headers.set("Api-Key", microserviceProperties.getFabrickApiKey());
        headers.set("Auth-Schema", microserviceProperties.getFabrickAuthSchema());
        headers.set("X-Time-Zone", microserviceProperties.getFabrickTimeZone());

        return headers;
    }

    private String getHost(String url) throws URISyntaxException {
        URI uri = new URI(url);
        String domain = uri.getHost();
        return domain.startsWith("www.") ? domain.substring(4) : domain;
    }

}
