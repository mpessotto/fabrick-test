package it.reply.fabrick.demo.fabrick.model.fabrickresponse;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FabrickApiError {

    @JsonProperty("code")
    private String code;
    @JsonProperty("description")
    private String description;
    @JsonProperty("params")
    private String params;


}
