
package it.reply.fabrick.demo.delegate;


import it.reply.fabrick.demo.fabrick.model.BalanceResponse;
import it.reply.fabrick.demo.model.GetTransactionResponse;
import it.reply.fabrick.demo.model.GetTransactionsRequest;
import org.springframework.http.ResponseEntity;

public interface AccountDelegate {

    ResponseEntity<BalanceResponse> getCashAccountBalance(String accountId) throws Exception;

    ResponseEntity<GetTransactionResponse> getCashAccountTransactions(String accountId, GetTransactionsRequest body) throws Exception;
}

