
package it.reply.fabrick.demo.delegate;

import it.reply.fabrick.demo.fabrick.model.CreateMoneyTransferRequest;
import it.reply.fabrick.demo.fabrick.model.CreateMoneyTransferResponse;
import org.springframework.http.ResponseEntity;


public interface MoneyTransferDelegate {

    ResponseEntity<CreateMoneyTransferResponse> createMoneyTransfer(String accountId,CreateMoneyTransferRequest body) throws Exception;

}

