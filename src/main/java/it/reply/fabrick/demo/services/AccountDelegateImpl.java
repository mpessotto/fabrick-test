
package it.reply.fabrick.demo.services;


import it.reply.fabrick.demo.fabrick.model.TransactionItem;
import it.reply.fabrick.demo.delegate.AccountDelegate;
import it.reply.fabrick.demo.fabrick.FabrickApis;
import it.reply.fabrick.demo.fabrick.model.BalanceResponse;
import it.reply.fabrick.demo.model.GetTransactionResponse;
import it.reply.fabrick.demo.model.GetTransactionsRequest;
import it.reply.fabrick.demo.utils.validator.FormattedDateMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;



public class AccountDelegateImpl implements AccountDelegate {

    private static final Logger log = LoggerFactory.getLogger(FabrickApis.class);
    private final FabrickApis fabrickApis;

    public AccountDelegateImpl(FabrickApis fabrickApis) {
        this.fabrickApis = fabrickApis;
    }


    @Override
    public ResponseEntity<BalanceResponse> getCashAccountBalance(String accountId) throws Exception {
        return new ResponseEntity<>(fabrickApis.getBalance(accountId), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<GetTransactionResponse> getCashAccountTransactions(String accountId, GetTransactionsRequest body) throws Exception {
        FormattedDateMatcher matcher = new FormattedDateMatcher();
        if (matcher.matches(body.getFromDate()) && matcher.matches(body.getToDate())) {
            GetTransactionResponse transactionResponse = new GetTransactionResponse();
            List<TransactionItem> transactionItemList =fabrickApis.getTransactions(accountId, body.getFromDate(), body.getToDate()).getList();
            int listSize = transactionItemList.size();
            transactionResponse.setTransactionTotal(listSize);
            if (body.getPageItem()!= null && body.getPageNumber()!= null && body.getPageNumber() <= (int)Math.round(Math.ceil(listSize / (float) body.getPageItem()))) {
                int start = (body.getPageItem() * body.getPageNumber())-body.getPageItem();
                int end = body.getPageItem() * body.getPageNumber() > listSize ? listSize: body.getPageItem() * body.getPageNumber();
                List<TransactionItem> sublist = transactionItemList.subList(start,end);
                transactionResponse.setTransactionItemList(sublist);
            } else {
                transactionResponse.setTransactionItemList(transactionItemList);
            }

            return new ResponseEntity<>(transactionResponse,HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}

