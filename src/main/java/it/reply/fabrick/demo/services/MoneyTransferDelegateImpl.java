
package it.reply.fabrick.demo.services;


import it.reply.fabrick.demo.fabrick.model.CreateMoneyTransferRequest;
import it.reply.fabrick.demo.delegate.MoneyTransferDelegate;
import it.reply.fabrick.demo.fabrick.FabrickApis;
import it.reply.fabrick.demo.fabrick.model.CreateMoneyTransferResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;



public class MoneyTransferDelegateImpl implements MoneyTransferDelegate {

    private final FabrickApis fabrickApis;

    public MoneyTransferDelegateImpl(FabrickApis fabrickApis) {
        this.fabrickApis = fabrickApis;
    }


    @Override
    public ResponseEntity<CreateMoneyTransferResponse> createMoneyTransfer(String accountId, CreateMoneyTransferRequest body) throws Exception {
        return new ResponseEntity<>(fabrickApis.createMoneyTransfer(accountId, body), HttpStatus.CREATED);
    }
}

