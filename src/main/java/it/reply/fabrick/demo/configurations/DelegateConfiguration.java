package it.reply.fabrick.demo.configurations;

import it.reply.fabrick.demo.delegate.AccountDelegate;
import it.reply.fabrick.demo.delegate.MoneyTransferDelegate;
import it.reply.fabrick.demo.fabrick.FabrickApis;
import it.reply.fabrick.demo.services.AccountDelegateImpl;
import it.reply.fabrick.demo.services.MoneyTransferDelegateImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DelegateConfiguration {

    private FabrickApis fabrickApis;

    public DelegateConfiguration(@Autowired FabrickApis fabrickApis) {
        this.fabrickApis = fabrickApis;
    }

    @Bean(name="accountDelegate")
    public AccountDelegate getAccountDelegate(){
        return new AccountDelegateImpl(fabrickApis);
    }

    @Bean(name="moneyTransferDelegate")
    public MoneyTransferDelegate getMoneyTransferDelegate(){
        return new MoneyTransferDelegateImpl(fabrickApis);
    }

}
