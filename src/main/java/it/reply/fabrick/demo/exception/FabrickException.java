package it.reply.fabrick.demo.exception;

public class FabrickException extends Exception{
    private final Throwable root;
    private final String messageErr;


    public FabrickException( Throwable root,String messageErr) {
        super(messageErr == null ? "N.A." : messageErr,root);
        this.root = root;
        this.messageErr = messageErr;
    }

    public FabrickException(String messageErr) {
        this(null,messageErr);
    }


    public Throwable getRoot() {
        return root;
    }


    public String getMessageErr() {
        return messageErr;
    }

}
