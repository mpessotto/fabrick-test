package it.reply.fabrick.demo.utils.converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class Converter {

	private Converter() {
	}

	public static Charset getCharset() {
		return StandardCharsets.UTF_8;
	}

	// byte to String
	public static String bytesToString(byte[] bs) {
		return new String(bs, getCharset());
	}

	// String to Byte
	public static byte[] stringToBytes(String in) {
		return in.getBytes(getCharset());
	}

}
