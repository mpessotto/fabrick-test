package it.reply.fabrick.demo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import it.reply.fabrick.demo.fabrick.model.TransactionItem;

import java.util.List;

public class GetTransactionResponse {

    @JsonProperty("transactionList")
    List<TransactionItem> transactionItemList;

    @JsonProperty("transactionTotal")
    Integer transactionTotal;

    public List<TransactionItem> getTransactionItemList() {
        return transactionItemList;
    }

    public void setTransactionItemList(List<TransactionItem> transactionItemList) {
        this.transactionItemList = transactionItemList;
    }

    public Integer getTransactionTotal() {
        return transactionTotal;
    }

    public void setTransactionTotal(Integer transactionTotal) {
        this.transactionTotal = transactionTotal;
    }
}
