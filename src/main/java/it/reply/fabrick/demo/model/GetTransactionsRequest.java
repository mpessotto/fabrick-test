package it.reply.fabrick.demo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;

public class GetTransactionsRequest {

    @JsonProperty("requestId")
    private String requestId = null;

    @JsonProperty("fromDate")
    @NotNull
    private String fromDate = null;

    @JsonProperty("toDate")
    @NotNull
    private String toDate = null;

    @JsonProperty("pageNumber")
    private Integer pageNumber = null;

    @JsonProperty("pageItem")
    private Integer pageItem = null;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public Integer getPageItem() {
        return pageItem;
    }

    public void setPageItem(Integer pageItem) {
        this.pageItem = pageItem;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }
}
